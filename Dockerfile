FROM itzg/minecraft-server:2024.7.2-java21
RUN curl -sfL https://raw.githubusercontent.com/igtm/envbed/master/install.sh | sh -s -- -b=/usr/local/bin
COPY --chmod=755 start /
RUN mkdir config
COPY DiscordSRV.yml /config/DiscordSRV.yml
